const express = require('express');
const router = express.Router();

const usersController = require('../controllers/usersController');

/* 
   >> Create 2 new routes and controllers in our userRoutes and userControllers.

    >> Create a route and controller to update a single user's username field to our input from a request body.
        -endpoint: '/:id'
        
*/

router.post('/', usersController.createUsers);
router.get('/users', usersController.getAllUsers);
router.post('/:id', usersController.findUserAndUpdate);
router.get('/getSingleUser/:id', usersController.getSingleUser);

module.exports = router;