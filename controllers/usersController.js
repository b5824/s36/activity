/* 
   >> Create a new POST method route to create 3 new user documents:
        -endpoint: "/users"
        -This route should be able to create a new user document.
        -Then, send the result in the client.
        -Catch an error while saving, send the error in the client.
        -STRETCH GOAL: no duplicate usernames
*/
const User = require('../models/User');

module.exports.createUsers = (req, res) => {
    console.log(req.body);

    User.findOne({ username: req.body.username })
        .then((result) => {
            // console.log(result);
            if (result !== null && result.username === req.body.username) {
                res.send('User already exists!');
            } else {
                let createNewUser = new User({
                    username: req.body.username,
                    password: req.body.password,
                });

                createNewUser
                    .save()
                    .then((result) => res.send(result))
                    .catch((err) => res.send(err));
            }
        })
        .catch((err) => res.send(err));
};

/* 
    >> Create a new GET method route to retrieve all user documents:
        -endpoint: "/users"
        -Then, send the result in the client.
        -Catch an error, send the error in the client.
*/

module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

/* 
-Create a new controller which is able to get the id from the url through the use of req.params:
            -Create an updates object and add the new value from our req.body as the new value to the username field.

            -Add a findByIdAndUpdate method from the User model and pass the id from req.params as its first argument.

            -Add the updates object as its second argument.

            -Add {new:true} as its third argument so the result would return the updated document instead of the old one.

            -Then pass the result to the client.

            -Catch the error and pass the error to the client.
*/

module.exports.findUserAndUpdate = (req, res) => {
    //get id from req.params
    const getId = req.params.id;
    const usernameUpdate = {
        username: req.body.username,
        password: req.body.password,
    };

    //use findByIdAndUpdate
    User.findByIdAndUpdate({ _id: getId }, usernameUpdate, { new: true })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

module.exports.getSingleUser = (req, res) => {
    User.findById({ _id: req.params.id })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

/* 
 >> Create a route and controller to get a single user.
            -endpoint: '/getSingleUser/:id'
            
            -Create a new controller which is able to get the id of the user from the url through the use of req.params.
                -In the controller, add a findById method from the User model and pass the id from req.params as its argument.
                -Then, send the result to the client.
                -Catch the error and send the error to the client.
*/