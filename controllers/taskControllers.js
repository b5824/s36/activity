//import the task model in our controllers. So that our controllers or controller functions may have access to our Task model
const Task = require('../models/Task');

// Create task
//end goal: Create task without duplicate names
module.exports.createTaskController = (req, res) => {
    console.log(req.body);
    //It calls the model and goes over each document and find whatever is inside the req.body called name
    Task.findOne({ name: req.body.name })
        //.then has a function inside. It's a form of promise, it enables the code to be asynchronous. A promise can either be a fulfilled promise or a failed promise.
        .then((result) => {
            console.log(result);
            //req.body is taken from postman. result.name is taken from the result found after Task.findOne() is executed.
            if (result !== null && result.name === req.body.name) {
                return res.send('Duplicate task found');
            } else {
                //If no result is found then we're creating an object and creates a new task, taking in values from the req.body.
                let newTask = new Task({
                    name: req.body.name,
                    status: req.body.status,
                });

                // We can then save the the newly created task to the database and will send the result back to the user IF it is successful. Otherwise, it will throw an error.
                newTask
                    .save()
                    .then((result) => res.send(result))
                    .catch((error) => res.send(error));
            }
        })
        .catch((error) => res.send(error));
};

// ! Get ALL tasks controller
module.exports.getAllTasksController = (req, res) => {
    //Will retrieve all the tasks added in the database
    Task.find({})
        .then((result) => {
            res.send(result);
        })
        .catch((err) => {
            res.send(err);
        });
};